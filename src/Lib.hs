{-# LANGUAGE OverloadedStrings #-}

module Lib where

import Data.ByteString.Builder
import Data.ByteString.Lazy (toStrict)
import Data.ByteString.Char8 as B
import System.Hardware.Serialport
import Data.Monoid ((<>))
import Data.Time as Time

-- Serial settings. Timeout is in 100ms: 10 * 2 = 200ms
openPort :: String -> IO SerialPort
openPort p = openSerial p defaultSerialSettings { commSpeed = CS2400, bitsPerWord = 8, stopb = One, parity = NoParity, timeout = 2 }

closePort :: SerialPort -> IO ()
closePort p = closeSerial p

recvData :: SerialPort -> IO B.ByteString
recvData p = recv p 2

bytesToHex :: B.ByteString -> B.ByteString
bytesToHex bs = (toStrict . toLazyByteString . byteStringHex) bs <> " "

-- Take time and bytes and print out
formatReceivedMessage :: UTCTime -> B.ByteString -> B.ByteString
formatReceivedMessage ts msg = 
    "\n" <> (B.pack $ show ts) <> ": " <> (bytesToHex msg)

recvMsg :: SerialPort -> Bool -> IO ()
recvMsg port timedOut= do
    -- This is impure stuff: <- means do the impure thing on the right and give me the result as variable on left
    received <- recvData port
    timestamp <- Time.getCurrentTime
    -- This is like an if then, same as if timedOut then (formatReceivedMessage timestamp received) else received
    -- But is more like a case statement so easier to expand
    let printReceived | timedOut  = formatReceivedMessage timestamp received
                      | otherwise = bytesToHex received
    -- See if received anything, if so print time and bytes or try again
    if (B.length received /= 0)
        then B.putStr printReceived <> recvMsg port False
        else recvMsg port True

recvLoop :: String -> IO ()
recvLoop p = do
    port <- openPort p
    Prelude.putStrLn $ "Listening on port: " ++ p
    recvMsg port True
    closePort port
