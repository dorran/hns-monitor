import Lib

import Test.Hspec
import Text.Printf (printf)

main :: IO ()
main = hspec $ do
  describe "Lib tests" $ do
    it "testFunc" $
      testFunc `shouldBe` "Some string"

  describe "Serial Port" $ do
    it "open /dev/ttyUSB0" $
      openPort "/dev/ttyUSB0" `shouldBe` "asd"
